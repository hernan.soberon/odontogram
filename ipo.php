<?php 
	$mysql_result = [
  	"event_id" => 11492,
	"name" => "Adriana Sifuentes",
	"showsPrimary" => intval('0'), 
	"comments_ipo" => "Estos son los comentarios", 
	"procedures_ipo" => '[{"tooth":18,"pro":101,"title":"Diente extraído","side":false},
			{"tooth":28,"pro":101,"title":"Diente extraído","side":false},
			{"tooth":38,"pro":101,"title":"Diente extraído","side":false},
			{"tooth":16,"pro":99,"title":"Placa","side":"top"},
			{"tooth":15,"pro":99,"title":"Placa","side":"top"},
			{"tooth":15,"pro":99,"title":"Placa","side":"right"},
			{"tooth":14,"pro":99,"title":"Placa","side":"top"},
			{"tooth":14,"pro":99,"title":"Placa","side":"left"},
			{"tooth":13,"pro":99,"title":"Placa","side":"top"},
			{"tooth":13,"pro":99,"title":"Placa","side":"right"},
			{"tooth":13,"pro":99,"title":"Placa","side":"left"},
			{"tooth":12,"pro":99,"title":"Placa","side":"top"},
			{"tooth":12,"pro":99,"title":"Placa","side":"right"},
			{"tooth":12,"pro":99,"title":"Placa","side":"left"},
			{"tooth":12,"pro":99,"title":"Placa","side":"bottom"},
			{"tooth":11,"pro":99,"title":"Placa","side":"top"},
			{"tooth":11,"pro":99,"title":"Placa","side":"right"},
			{"tooth":11,"pro":99,"title":"Placa","side":"left"},
			{"tooth":11,"pro":99,"title":"Placa","side":"bottom"},
			{"tooth":21,"pro":99,"title":"Placa","side":"top"},
			{"tooth":21,"pro":99,"title":"Placa","side":"right"},
			{"tooth":21,"pro":99,"title":"Placa","side":"left"},
			{"tooth":21,"pro":99,"title":"Placa","side":"bottom"},
			{"tooth":22,"pro":99,"title":"Placa","side":"top"},
			{"tooth":22,"pro":99,"title":"Placa","side":"right"},
			{"tooth":22,"pro":99,"title":"Placa","side":"left"},
			{"tooth":22,"pro":99,"title":"Placa","side":"bottom"},
			{"tooth":24,"pro":99,"title":"Placa","side":"top"},
			{"tooth":26,"pro":99,"title":"Placa","side":"left"}]',
	];
 ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>History Clinical - Índice de Placa O’leary</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="./vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="./vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="./css/style.css?1">
  <link rel="stylesheet" href="./css/custom.css?2">
  <link rel="stylesheet" href="./css/odontogram.css?1">
  <link rel="stylesheet" href="./css/print.css?1">
  <!-- endinject -->
  <link rel="shortcut icon" href="./images/favicon.png" />
  <style type="text/css">
  	.tooth:hover .tooth-group{
  		z-index: 1;
  	}

  	
  </style>
</head>

<body>
  <div>
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a href="./" class="navbar-brand brand-logo"><img src="./images/logo.png?1561790190" alt="logo"></a>    <a href="/Hector/historyclinical-web/admin" class="navbar-brand brand-logo-mini"><img src="./images/logo-mini.png?1562122710" alt="logo"></a>  </div>

  <div class="navbar-menu-wrapper d-flex align-items-stretch">

    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item nav-profile dropdown">
        <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          <div class="nav-profile-icon">
            <i class="mdi mdi-account-circle icon-md"></i>
          </div>
          <div class="nav-profile-text">
            <p class="mb-1 text-black">Odontólogo Prueba</p>
          </div>
        </a>
        <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
		  <a href="#full" class="dropdown-item" id="fullscreen-button"><i class="mdi mdi-fullscreen mr-2 text-info"></i> Pantalla Completa</a>          
          <div class="dropdown-divider"></div>
          <a href="./" class="dropdown-item"><i class="mdi mdi-account mr-2 text-success"></i> Perfil</a>          
          <div class="dropdown-divider"></div>
          <a href="./" class="dropdown-item"><i class="mdi mdi-logout mr-2 text-primary"></i>Salir</a></div>
      </li>
     
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-menu"></span>
    </button>
  </div>
</nav>
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel w-100">
        <div class="content-wrapper">
          <header class="main-title">
          	<h1 class="top-round-borders">Índice de Placa O’leary: <small class="text-secondary"><?= $mysql_result['name']; ?></small></h1>
          	<div class="clearfix"></div>
          	<div class="row bg-white p-3 text-center">
          		<div class="col-3" title="Número de dientes disponibles"><b>Dientes: <br>
          			<big id="teeth_count"></big></b>
          		</div>
          		<div class="col-3" title="Número total de superficies disponibles"><b>Total Superficies: <br>
          			<big id="sides_count"></big></b>
          		</div>
          		<div class="col-3" title="Superficies manchadas"><b>Control: <br>
          			<big id="plaques_count"></big></b>
          		</div>
          		<div class="col-3" title="Índice de Placa O'lerly"><b>IP: <br>
          			<big  class="text-success"><span id="ipo_result"></span> %</big></b>
          		</div>
          	</div>
          </header>
          <form action="./index.php">
          <div class="row">
		    <div class="col-md-12 grid-margin stretch-card">
		      <div class="card">
		      	
		        <div class="card-body">
	        	  <div id="odontogram">
  	        		<div class="clearfix">
 		          	  <h5 class="label">Vestibular</h5>
 		            </div>
 		            <div class="cuadrant">
 		          	  <div class="tooth" id="tooth_18" data-id="18">
 		          		  <label>18</label>
 		          		  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_17" data-id="17">
 		          		  <label>17</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_16" data-id="16">
 		          		  <label>16</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_15" data-id="15">
 		          		  <label>15</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_14" data-id="14">
 		          		  <label>14</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_13" data-id="13">
 		          		  <label>13</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_12" data-id="12">
 		          		  <label>12</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_11" data-id="11">
 		          		  <label>11</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="clearfix"></div>
 		          	  
 		            </div>
 		            <div class="cuadrant">
 		          	  <div class="tooth" id="tooth_21" data-id="21">
 		          		  <label>21</label>
 		          		  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_22" data-id="22">
 		          		  <label>22</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_23" data-id="23">
 		          		  <label>23</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_24" data-id="24">
 		          		  <label>24</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_25" data-id="25">
 		          		  <label>25</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_26" data-id="26">
 		          		  <label>26</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_27" data-id="27">
 		          		  <label>27</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_28" data-id="28">
 		          		  <label>28</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="clearfix"></div>
 		          	  
 		            </div>
 				    <div class="clearfix">
 				    	<h5 class="label">
	 			            <span class="right-label">Mesial</span>
	 			            <span class="center-label">Lingual</span>
	 			            <span class="left-label">Distal</span>
 			            </h5>
 		            </div>
 		            <div class="cuadrant">
		              
 		          	  <div class="tooth" id="tooth_48" data-id="48">
 		          		  <label>48</label>
 		          		  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_47" data-id="47">
 		          		  <label>47</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_46" data-id="46">
 		          		  <label>46</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_45" data-id="45">
 		          		  <label>45</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_44" data-id="44">
 		          		  <label>44</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_43" data-id="43">
 		          		  <label>43</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_42" data-id="42">
 		          		  <label>42</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_41" data-id="41">
 		          		  <label>41</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		            </div>
 		            <div class="cuadrant">
 		              
 		          	  <div class="tooth" id="tooth_31" data-id="31">
 		          		  <label>31</label>
 		          		  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_32" data-id="32">
 		          		  <label>32</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_33" data-id="33">
 		          		  <label>33</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_34" data-id="34">
 		          		  <label>34</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_35" data-id="35">
 		          		  <label>35</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_36" data-id="36">
 		          		  <label>36</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_37" data-id="37">
 		          		  <label>37</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		          	  <div class="tooth" id="tooth_38" data-id="38">
 		          		  <label>38</label>
						  <div class="tooth-group">
 		          		  	<div class="side side_top" data-side="top"></div>
							<div class="side side_left" data-side="left"></div>
							
							<div class="side side_right" data-side="right"></div>
							<div class="side side_bottom" data-side="bottom"></div>
 		          		  </div>
 		          	  </div>
 		            </div>
 		            <div class="clearfix">
 			            <div class="label">
 			          	  <h5>Vestibular</h5>
 			            </div>
 		            </div>

	        	  </div>
	        	  <!-- ./Odontogram -->

	        	  <h3>Notas:</h3>
	        	  <p>   - Haz click sobre la cara del diente para marcarla manchada. 
	        	  	<br>- Haz click sobre la cara marcada para desmarcarla.
	        	  	<br>- Click sobre el número del diente para identificarlo como extraído.
	        	  	<br>- Click nuevamente sobre el diente extraído para habilitarlo de nuevo. 
	        	  </p>
		        </div>
		      </div>
		    </div>

		    <div class="col-md-12 grid-margin stretch-card">
		      <div class="card">
		      	<div class="card-body">
		      		<div class="mb-5 d-none">
		      			<label for="procedures_ipo" class="text-danger">Cambios</label>
		      			<textarea name="procedures_ipo" id="procedures_ipo" class="form-control full-textarea" rows="10" >[]</textarea>
		      		</div>
		      		<label for="comments_ipo" class="text-info">Comentarios</label>
		      		<textarea name="comments_ipo" id="comments_ipo" class="form-control full-textarea"  cols="30" rows="10"></textarea>
		      	</div>
		      </div>
		    </div>
		    <div class="col-md-12">
		    	<button type="submit" class="btn btn-success">Guardar</button>
		    	<a href="./index.php" class="btn btn-info float-right index">Volver al Odontograma</a>
		    </div>
		  </div>
		  <!-- ./row -->
		  </form>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
		<footer class="footer">
		  <div class="d-sm-flex justify-content-center justify-content-sm-between">
		    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.historyclinical.com" target="_blank" rel="nofollow">History Clinical</a>. All rights reserved.</span>
		  </div>
		</footer>
		<!-- partial -->
      </div>
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  


  <!-- <script src="./js/main.js"></script> -->
  <script src="./js/ipo.js"></script>

  <!-- plugins:js -->
  <script src="./vendors/js/vendor.bundle.base.js"></script>
  <script src="./vendors/js/vendor.bundle.addons.js"></script>
  <!-- endplugins -->

  

  
  <!-- inject:js -->
  <script src="./js/off-canvas.js"></script>
  <script src="./js/misc.js"></script>
  <!-- endinject -->
  

  <script type="text/javascript">
	$( document ).ready(function() {

		//START THE ODONTOGRAM 
	    let odontogram = new Odontogram();

	  	$.ajax({
			type: "GET",
			url: "./js/procedures.json", // All procedures
			success: function(initialProcedures) {
				odontogram.procedures = initialProcedures;
				odontogram.config = <?= json_encode($mysql_result); ?>;
				//Start the initial count
	  		olerly();
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
		        alert("Status: " + textStatus); 
		        alert("Error: " + errorThrown); 
		    }  
		});

	  	


	});

	</script>

 

</body>

</html>
