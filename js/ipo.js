/** 
 * Odontogram class definition for 
 * History Clinical 
 * By H'Soberon 
 * @ Glorfindel 
 * 2019
 */ 

 

"use strict";


class Odontogram {
	// static showsPrimaryProcedures = false;
	constructor(initialConfig){

		this._procedures = [];
		this._config = [];

		let odontogram = this;

		
		// Mark the side of the tooth on click
	  	$('.tooth .side').not('.pro_exodontics').click(function(event) {
	  		event.preventDefault();
	  		event.stopPropagation();

	  		//validate if teeth has been removed
	  		let removedTooth = $(this).closest('.tooth').hasClass('pro_exodontics');
	  		
	  		if(!removedTooth){
	  			markSide($(this));	
	  			let marked = $(this).hasClass('pro_caries');
	  			if(!marked){
		  			odontogram.procedures[99].activate();
	  			}else{
	  				clearSideProcedures(false, this); //no confirmation
	  			}
	  			
	  		}

	  		//call the recount 
	  		olerly();
	  	});

	  	// Mark the entire tooth on click
	  	$('.tooth').click(function(event) {
	  		event.preventDefault();

	  		let removedTooth = $(this).hasClass('pro_exodontics');
	  		
	  		if(!removedTooth){
	  			if(confirm('¿Marcar diente como removido?')){
	  				markTooth($(this));
	  				odontogram.procedures[101].activate();	
	  			}
	  		}else if(confirm('¿Habilitar de nuevo el diente?')){
	  			markTooth($(this));
	  			clearToothProcedures(false);
	  		}

	  		//call the recount 
	  		olerly();
	  		
	  	});
	  	


	}


	//Stablish all the procedures for the sistem
	set procedures(initialProcedures){
		console.log('Loading all procedures');
		let pro = [];

		initialProcedures.map(function(item) {
			pro[item.key] = new Procedure(item.key, item.title, item.className, item.type, item.apply, item.clearBefore);
			pro[item.key].active;
		});
		this._procedures = pro;
	}

	//return all procedures
	get procedures(){
		return this._procedures;
	}

	// Setter for the initial config
	set config(initialConfig) {
		console.log('Loading initial config');
		this._config = initialConfig;

		// shows the Primary Dentity section
		if(this._config.showsPrimary) {
			togglePrimaryTeethSection();
			$('.primary-toggle').prop('checked', true);
		}

		// Fill the comments section
		if(this._config.comments_ipo) {
			$('#comments_ipo').val(this._config.comments_ipo);
		}

		// Display the initial procedures
		if(this._config.procedures_ipo && JSON.parse(this._config.procedures_ipo).length > 0) {
			let initialProcedures = JSON.parse(this._config.procedures_ipo);
			for (var i = initialProcedures.length - 1; i >= 0; i--) {
				let this_pro = initialProcedures[i];
				let tooth = $('#tooth_' + this_pro.tooth);
				let side = (this_pro.side) ? tooth.find('.side_'+this_pro.side) : false;
				this._procedures[this_pro.pro].activate(tooth, side);
			}	
		}
	}



	// set changes(newChanges) {
	// 	if(newChanges.length > 1){
	// 		for (var i = newChanges.length - 1; i >= 0; i--) {
	// 			this._changes.push(newChanges[i]);	
	// 		}
	// 	}else{
	// 		this._changes.push(newChanges);	
	// 	}
		
	// 	console.log('New changes mades');
		
	// 	$('#procedures_ipo').val(JSON.stringify(this._changes));
	// }

	// get changes() {
	// 	return this._changes;
	// }


	
}








 function Procedure(key, title, className, type, apply, clearBefore) {
	this.key = key;
	this.title = title;
	this.className = className;
	this.type = type;
	this.apply = apply;
	this.clearBefore = clearBefore;

 	
 	this.activate = function(tooth, sides) {
 		
 		if(!tooth){
 			tooth = $('#odontogram .tooth.active');
 		}

 		if(!sides) {
 			sides = $('#odontogram .side.active');
 		}

 		
		if(this.className == 'pro_angle' || this.className == 'pro_angle_done'){
			let thisPro = this;
			$(sides).each(function () {
				if($(this).data('side') == 'left' || $(this).data('side') == 'right'){
					if(thisPro.clearBefore) {clearSideProcedures(false, this);}
					$(this).addClass(thisPro.className);
					addChanges({
						"tooth" : tooth.data('id'), 
						"pro" : thisPro.key, 
						"title": thisPro.title,
						"side" : $(this).data('side')
					});
				}
			});
		}else if(this.className == 'pro_restoration' || this.className == 'pro_restoration_done'){
			let thisPro = this;
			$(sides).each(function () {
				if($(this).data('side') == 'top' || $(this).data('side') == 'bottom'){
					if(thisPro.clearBefore) {clearSideProcedures(false, this);}
					$(this).addClass(thisPro.className);
					addChanges({
						"tooth" : tooth.data('id'), 
						"pro" : thisPro.key, 
						"title": thisPro.title,
						"side" : $(this).data('side')
					});
				}
			});
		}else if(this.apply == 'side'){
 			let thisPro = this;
 			$(sides).each(function () {
 				if(thisPro.clearBefore) {clearSideProcedures(false, this);}
 				$(this).addClass(thisPro.className);
 				addChanges({
					"tooth" : tooth.data('id'), 
					"pro" : thisPro.key, 
					"title": thisPro.title,
					"side" : $(this).data('side')
				});
 			});
 		}else{
 			if(this.clearBefore) {clearToothProcedures(false, tooth);}
 			tooth.addClass(this.className);
 			addChanges({
					"tooth" : tooth.data('id'), 
					"pro" : this.key, 
					"title": this.title,
					"side" : false
				});
 		}

 		console.log(this.className);
 	};
}



function addChanges(newChanges = []) {
	
	let oldChanges = JSON.parse($('#procedures_ipo').val());
	oldChanges.push(newChanges)
	$('#procedures_ipo').val(JSON.stringify(oldChanges));
}




//clear the selected tooth of all procedures
function clearToothProcedures(with_confirm = true, tooth = false) {
	if(!with_confirm || confirm('Está seguro de borrar todos los procesos sobre este diente?')){
		if(!tooth) {
			tooth = $('#odontogram .tooth.active');
		}
		let sides = $(tooth).find('.side');
		//remove tooth classes
		$(tooth).removeClass (function (index, className) {
		    return (className.match (/(^|\s)pro_\S+/g) || []).join(' ');
		});
		//remove tooth procedures
		let changes = JSON.parse($('#procedures_ipo').val());
		let findings = [];
		for (var i = changes.length - 1; i >= 0; i--) {
			if(changes[i].tooth == $(tooth).data('id')){
				changes.splice(i,1);
			}
		}
		// changes.forEach(function(change, index){
		// 	if(change.tooth == $(tooth).data('id')){
		// 		changes.splice(index,1);
		// 	}
		// });	
		//update changes
		$('#procedures_ipo').val(JSON.stringify(changes));

		//remove sides procedures
		clearSideProcedures(false, sides);
		$('#odontogram .active').removeClass('active');
	}
}


//clear the selected sides of all procedures
function clearSideProcedures(with_confirm = true, sides = false) {
	if(!with_confirm || confirm('Está seguro de borrar todos los procesos sobre este cara?')){
		if(!sides) {sides = $('#odontogram .tooth.active .side.active');}
		//remove sides classes
		$(sides).removeClass (function (index, className) {
		    return (className.match (/(^|\s)pro_\S+/g) || []).join(' ');
		});
		//remove side procedures form changes
		let changes = JSON.parse($('#procedures_ipo').val());
		$(sides).each(function () {
			let side = this;
			changes.forEach(function(change, index){
				if(change.tooth == $(side).closest('.tooth').data('id') && 
						change.side == $(side).data('side')){
							changes.splice(index,1);
				}
			});	
		});
		
		$('#procedures_ipo').val(JSON.stringify(changes));
		$('#odontogram .active').removeClass('active');
	}
}



/**  *** THOOTHS *** */


//Mark this side and its tooth as active
function markSide(side) {
	if($(side).hasClass('active')){
		$(side).removeClass('active');
	}else{
		$(side).addClass('active');	
		markTooth($(side).closest('.tooth'));
	}
}

//Mark the tooth  as active
function markTooth(tooth) {
	$(tooth).addClass('active');
}



/**  *** CALCULATE O'LEARY PLAQUE INDEX  *** */

function olerly() {
	const sides_x_tooth = 4;
	const teeth_total = $('.tooth').length;
	const teeth_remove = $('.pro_exodontics').length;
	const teeth = teeth_total - teeth_remove;
	const sides = teeth * sides_x_tooth;

	const plaques = $('.side.pro_caries').length;

	const IPO = Math.round(plaques * 100.0 / sides);

	$('#teeth_count').text(teeth);
	$('#sides_count').text(sides);
	$('#plaques_count').text(plaques);
	$('#ipo_result').text(IPO);

}



